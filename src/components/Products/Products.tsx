import { Grid } from "@material-ui/core";
import React, {useState, useEffect} from "react";
import Product from "./Product/Product";

import useStyles from "./productsStyles";



interface Props {
  products: never[];
  onAddToCart: (productId: string, quantity: number) => Promise<void>;
}

const Products : React.FC<Props> = (props: Props) => {
  const classes = useStyles();

  console.log(props.products)
  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      <Grid container justify="center" spacing={4}>
        {props.products.map((product : any) => (
          <Grid item key={product.id} xs={12} sm={6} md={4} lg={3}>
            <Product product={product} onAddToCart={props.onAddToCart}/>
          </Grid>
        ))}
      </Grid>
    </main>
  );
};

export default Products;
