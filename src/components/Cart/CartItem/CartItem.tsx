import React from "react";
import {
  Typography,
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
} from "@material-ui/core";

import useStyles from "./cartItemStyles";

interface Props {
  item: any;
  handleUpdateCartQty: (productId: string, quantity: number) => Promise<void>;
  handleRemoveFromCart: (productId: string) => Promise<void>;
}

const CartItem: React.FC<Props> = (props: Props) => {
  const classes = useStyles();

  return (
    <Card>
      <CardMedia
        image={props.item.media.source}
        title={props.item.name}
        className={classes.media}
      />
      <CardContent className={classes.cardContent}>
        <Typography variant="h4">{props.item.name}</Typography>
        <Typography variant="h5">
          {props.item.line_total.formatted_with_symbol}
        </Typography>
      </CardContent>
      <CardActions className={classes.cartActions}>
        <div className={classes.buttons}>
          <Button
            type="button"
            size="small"
            onClick={() =>
              props.handleUpdateCartQty(props.item.id, props.item.quantity - 1)
            }
          >
            -
          </Button>
          <Typography>{props.item.quantity}</Typography>
          <Button
            type="button"
            size="small"
            onClick={() =>
              props.handleUpdateCartQty(props.item.id, props.item.quantity + 1)
            }
          >
            +
          </Button>
        </div>
        <Button variant="contained" type="button" color="secondary"  onClick={() => props.handleRemoveFromCart(props.item.id)}>
          Eliminar
        </Button>
      </CardActions>
    </Card>
  );
};

export default CartItem;
