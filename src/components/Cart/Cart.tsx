import React from "react";
import { Container, Typography, Button, Grid } from "@material-ui/core";
import { Link } from "react-router-dom";
import CartItem from "./CartItem/CartItem";

import useStyles from "./cartStyles";

interface Props {
  cart: any;
  handleUpdateCartQty: (productId: string, quantity: number) => Promise<void>;
  handleRemoveFromCart: (productId: string) => Promise<void>;
  handleEmptyCart: () => Promise<void>;
}

const Cart = (props: Props) => {
  const classes = useStyles();

  const EmptyCart = () => {
    return (
      <Typography variant="subtitle1" component={Link} to="/">
        No tienes libros en tu carrito, ¡empieza a añadir algunos!
      </Typography>
    );
  };

  const FilledCart = () => {
    return (
      <>
        <Grid container spacing={3}>
          {props.cart.line_items.map((item: any) => (
            <Grid item xs={12} sm={4} key={item.id}>
              <CartItem
                item={item}
                handleRemoveFromCart={props.handleRemoveFromCart}
                handleUpdateCartQty={props.handleUpdateCartQty}
              />
            </Grid>
          ))}
        </Grid>
        <div className={classes.cardDetails}>
          <Typography variant="h4">
            Subtotal: {props.cart.subtotal.formatted_with_symbol}
          </Typography>
          <div>
            <Button
              className={classes.emptyButton}
              size="large"
              type="button"
              variant="contained"
              color="secondary"
              onClick={props.handleEmptyCart}
            >
              Vaciar carrito
            </Button>
            <Button
              className={classes.checkoutButton}
              size="large"
              type="button"
              variant="contained"
              color="primary"
            >
              Checkout
            </Button>
          </div>
        </div>
      </>
    );
  };

  if (!props.cart.line_items) return <div>Loading</div>;

  return (
    <Container>
      <div className={classes.toolbar} />
      <Typography className={classes.title} variant="h3" gutterBottom>
        Tu carrito
      </Typography>
      {!props.cart.line_items.length ? <EmptyCart /> : <FilledCart />}
    </Container>
  );
};

export default Cart;
